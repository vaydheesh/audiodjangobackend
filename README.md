![CI/CD Pipeline Status](https://gitlab.com/vaydheesh/audiodjangobackend/badges/master/pipeline.svg)

# Problem Statement
- [Click Here](ProblemStatement.pdf)

# Installation
- `pipenv` can be used to install the development dependencies and enter a shell:
    ```
    pip install pipenv
    pipenv install
    pipenv shell
    ```
  

- Start Server
  ```
  cd AudioAPI
  python manage.py runserver
  ```

- Unittests
  ```
  cd AudioAPI
  python manage.py test api.tests
  ```
  

# CI/CD Status
- [Gitlab pipeline](https://gitlab.com/vaydheesh/audiodjangobackend/-/pipelines)

# Documentation
## Database Choice:
    - SQLite 3 Database
    - Many to Many relationship to store list of participants

## Schema:
### Song:
    - id: AutoField(primary_key=True)
    - duration: PositiveIntegerField()
    - upload: DateTimeField(auto_now=True)
    - name: CharField(max_length=100)


### Audiobook:
    - id AutoField(primary_key=True)
    - duration: PositiveIntegerField()
    - upload DateTimeField(auto_now=True)
        - auto_now: set the field to current time every time the object is saved
        - cannot be in the past

    - title: CharField(max_length=100)
    - author: CharField(max_length=100)
    - narrator: CharField(max_length=100)


### Podcast:
    - id: AutoField(primary_key=True)
    - duration: PositiveIntegerField()
    - upload: DateTimeField(auto_now=True)
    - name: CharField(max_length=100)
    - host: CharField(max_length=100)
    - participants: ManyToManyField (blank=True)
        - Default Nested Serializer doesn't allow writing nested objects to db
        - during object creation
        - This is resolved by using WritableNestedModelSerializer


### Participant:
    - id: AutoField(primary_key=True)
    - name: CharField(max_length=100)
    

## Packages Used
    - Django and DjangoRestFramework
    - drf-writable-nested:
        - Default Nested Serializer doesn't allow writing nested objects during object creation
        - This is resolved by using WritableNestedModelSerializer class


## API endpoints:
    - "api/<str:audioFileType>/<int:audioFileID>/": GET, PUT, DELETE
      - Get, Update, Delete records
    - "api/<str:audioFileType>": GET
      - GET all the records of given audioFileType
    - "api/": POST
      - Create records

## Example:
### Song
#### Get All: GET request to
    - http://127.0.0.1:8080/api/Song/

#### Get one record: GET request to
    - http://127.0.0.1:8080/api/Song/1/

#### Create record: POST request
      - http://127.0.0.1:8080/api/
      - payload 
        ```
        {
            "audioFileType": "Song",
            "audioFileMetadata": {
            "name": "Test Song 4",
            "duration": 10
            }
        }
        ```

#### Update record: PUT request
      - http://127.0.0.1:8080/api/Song/1/
      - payload 
        ```
        {
            "audioFileType": "Song",
            "audioFileMetadata": {
            "name": "Test Song 2",
            "duration": 10
        }
        ```

#### Delete record: DELETE request
      - http://127.0.0.1:8080/api/Song/2/

### Audiobook
#### Get All: GET request to
          - http://127.0.0.1:8080/api/Audiobook/
        
#### Get one record: GET request to
          - http://127.0.0.1:8080/api/Audiobook/1/
  
#### Create record: POST request
    - http://127.0.0.1:8080/api/
    - payload 
    ```
    {
        "audioFileType": "Audiobook",
        "audioFileMetadata": {
        "title": "Test Audiobook 2",
        "duration": 15,
        "author": "host 1",
            "narrator": "narrator 1"
        }
    }
    
    ```
  
#### Update record: PUT request
    - http://127.0.0.1:8080/api/Audiobook/1/
    - payload 
    ```
    {
        "audioFileType": "Audiobook",
        "audioFileMetadata": {
        "title": "Audiobook 4",
        "duration": 19,
        "author": "host 4",
            "narrator": "narrator 4"
        }
    ```
    
#### Delete record: DELETE request
    - http://127.0.0.1:8080/api/Audiobook/2/
    
### Podcast
#### Get All: GET request to
    - http://127.0.0.1:8080/api/Podcast/
        
#### Get one record: GET request to
    - http://127.0.0.1:8080/api/Podcast/1/
  
#### Create record: POST request
    - http://127.0.0.1:8080/api/
    - payload 
    ```
    {
        "audioFileType": "Podcast",
        "audioFileMetadata": {
        "name": "Test podcast 1",
        "duration": 12,
        "host": "host 1",
        "participants": [
                {"name": "participant-1"},
                {"name": "participant-2"},
                {"name": "participant-3"},
                {"name": "participant-4"},
                {"name": "participant-5"}
            ]
        }
    
    ```
  
#### Update record: PUT request
    - http://127.0.0.1:8080/api/Podcast/1/
    - payload 
    ```
    {
        "audioFileType": "Podcast",
        "audioFileMetadata": {
        "name": "Update podcast 3",
        "duration": 123,
        "host": "host 3",
        "participants": [
                {"name": "participant-6"},
                {"name": "participant-7"},
                {"name": "participant-8"},
            ]
        }
    ```

#### Delete record: DELETE request
    - http://127.0.0.1:8080/api/Podcast/2/
    

 
### Note
- [Insomnia](https://insomnia.rest/) exports file is available in root of project to test the REST APIs

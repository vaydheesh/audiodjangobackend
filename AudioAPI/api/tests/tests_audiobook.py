import json

from django.test import TestCase
from rest_framework import status

from .. import ConstInt
from ..models import Audiobook
from ..serializers import AudiobookSerializer


class AudiobookGetTestCase(TestCase):
    """
    Test the song endpoints.
    """

    def setUp(self):
        self.audio1 = Audiobook.objects.create(title='Audiobook 1', author='author 1', narrator='narrator 1', duration=10)
        self.audio2 = Audiobook.objects.create(title='Audiobook 2', author='author 2', narrator='narrator 2', duration=20)
        self.audio3 = Audiobook.objects.create(title='Audiobook 3', author='author 3', narrator='narrator 3', duration=30)
        self.audio4 = Audiobook.objects.create(title='Audiobook 4', author='author 4', narrator='narrator 4', duration=40)


    # http://localhost:8000/api/Audiobook/
    def test_get_all(self):
        url = '/api/Audiobook/'
        response = self.client.get(url, format='json')
        song = Audiobook.objects.all()
        serializer = AudiobookSerializer(song, many=True)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # http://localhost:8000/api/Audiobook/1
    def test_get_single(self):
        url = f'/api/Audiobook/{self.audio1.id}/'
        response = self.client.get(url, format='json')
        song = Audiobook.objects.get(id=self.audio1.id)
        serializer = AudiobookSerializer(song)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_id(self):
        url = f'/api/Audiobook/{9999}/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class AudiobookCreateTestCase(TestCase):
    """
    Test the song POST endpoints.

    http://localhost:8000/api/
    """

    def setUp(self):
        self.url = '/api/'
        self.valid_payload = {
            "audioFileType": "Audiobook",
            "audioFileMetadata": {
                "title": "Audiobook 11",
                "author": "host 11",
                "narrator": "narrator 11",
                "duration": 11,
            }
        }

    def test_create_valid_audio(self):
        response = self.client.post(
            self.url,
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        title = self.valid_payload["audioFileMetadata"]["title"]
        audio = Audiobook.objects.get(title=title)
        serializer = AudiobookSerializer(audio)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_file_type(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileType"] = "Audiobooks"
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_title_empty(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["title"] = ""
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_title_too_big(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["title"] = "A" * (ConstInt.str_len + 1)
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_invalid_duration(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["duration"] = -1
        response = self.client.post(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class AudiobookUpdateTestCase(TestCase):
    """
    Test the song POST endpoints.

    http://localhost:8000/api/
    """

    def setUp(self):
        self.audio1 = Audiobook.objects.create(title='Audiobook 1', author='author 1', narrator='narrator 1', duration=10)
        self.url = f'/api/Audiobook/{self.audio1.id}/'
        self.valid_payload = {
            "audioFileType": "Audiobook",
            "audioFileMetadata": {
                "title": "Audiobook 21",
                "author": "host 21",
                "narrator": "narrator 21",
                "duration": 21,
            }
        }

    def test_update_valid_song(self):
        response = self.client.put(
            self.url,
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        song = Audiobook.objects.get(id=self.audio1.id)
        serializer = AudiobookSerializer(song)

        self.assertJSONEqual(str(response.content, encoding='utf8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_file_type(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileType"] = "Audiobooks"
        response = self.client.put(
            f'/api/{invalid_payload["audioFileType"]}/{self.audio1.id}/',
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_title_empty(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["title"] = ""
        response = self.client.put(
            self.url,
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_title_too_big(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["title"] = "A" * (ConstInt.str_len + 1)
        response = self.client.put(
            self.url.format(self.audio1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_invalid_duration(self):
        invalid_payload = self.valid_payload
        invalid_payload["audioFileMetadata"]["duration"] = -1
        response = self.client.put(
            self.url.format(self.audio1.id),
            data=json.dumps(invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class AudiobookDeleteTestCase(TestCase):
    """
    Test the song endpoints.
    """

    def setUp(self):
        self.song1 = Audiobook.objects.create(title='Audiobook 1', duration=10)
        self.song2 = Audiobook.objects.create(title='Audiobook 2', duration=20)
        self.song3 = Audiobook.objects.create(title='Audiobook 3', duration=30)
        self.song4 = Audiobook.objects.create(title='Audiobook 4', duration=40)

    # http://localhost:8000/api/Audiobook/1
    def test_delete_single(self):
        url = f'/api/Audiobook/{self.song1.id}/'
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_id(self):
        url = f'/api/Audiobook/{9999}/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

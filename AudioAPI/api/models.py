from django.db import models

from . import ConstInt


class AbstractAudio(models.Model):
    """
    Abstract Audio Model

    All the audio files have common properties:
    duration and upload time
    """
    duration = models.PositiveIntegerField(blank=False)
    upload = models.DateTimeField(auto_now=True)
    # auto_now: set the field to current time every time the object is saved

    class Meta:
        abstract = True


class Audiobook(AbstractAudio):
    title = models.CharField(max_length=ConstInt.str_len, blank=False)
    author = models.CharField(max_length=ConstInt.str_len, blank=False)
    narrator = models.CharField(max_length=ConstInt.str_len, blank=False)


class Song(AbstractAudio):
    name = models.CharField(max_length=ConstInt.str_len, blank=False)


class Participant(models.Model):
    """
    Many to Many relation with Podcast to store list of participants
    """
    name = models.CharField(max_length=ConstInt.str_len, blank=False)


class Podcast(AbstractAudio):
    name = models.CharField(max_length=ConstInt.str_len, blank=False)
    host = models.CharField(max_length=ConstInt.str_len, blank=False)
    participants = models.ManyToManyField(
        Participant,
        blank=True,
    )

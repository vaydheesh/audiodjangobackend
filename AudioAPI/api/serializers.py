from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers

from . import ConstInt
from .models import Audiobook
from .models import Participant
from .models import Podcast
from .models import Song


class AudiobookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audiobook
        fields = (
            'id',
            'title',
            'duration',
            'upload',
            'author',
            'narrator',
        )


class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Song
        fields = (
            'id',
            'name',
            'duration',
            'upload',
        )


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ['name']


class PodcastSerializer(WritableNestedModelSerializer):
    """
    Default Nested Serializer doesn't allow writing nested objects
    during object creation
    This is resolved by inheriting from WritableNestedModelSerializer

    """
    participants = ParticipantSerializer(many=True)

    class Meta:
        model = Podcast
        fields = (
            'id',
            'name',
            'duration',
            'upload',
            'host',
            'participants',
        )

    def validate_participants(self, value):
        """
        Ensure only ConstInt.participant_len is allowed in the participants array
        """
        if len(value) > ConstInt.participant_len:
            raise serializers.ValidationError({
                "max participants":
                    f"max participants allowed are {ConstInt.participant_len}"
            })

        return value

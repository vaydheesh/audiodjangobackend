from . import ConstStr
from .models import Audiobook
from .models import Podcast
from .models import Song
from .serializers import AudiobookSerializer
from .serializers import PodcastSerializer
from .serializers import SongSerializer


# Maintain a dictionary between the audioFileType
# and respective Model
# This prevents if-else ladder during audioFileType
# And provides an abstraction over all the available models
ModelMap = {
    ConstStr.audiobook: Audiobook,
    ConstStr.podcast: Podcast,
    ConstStr.song: Song,
}

# Maintain a dictionary between the audioFileType
# and respective Model Serializer
# Same usage as ModelMap
SerializerMap = {
    ConstStr.audiobook: AudiobookSerializer,
    ConstStr.podcast: PodcastSerializer,
    ConstStr.song: SongSerializer,
}

from django.urls import path

from .views import audio_get_all
from .views import audio_post
from .views import AudioResponse

urlpatterns = [
    path('<str:audioFileType>/<int:audioFileID>/', AudioResponse.as_view(), name='audio_get'),
    path('', audio_post, name='audio_post'),
    path('<str:audioFileType>/', audio_get_all, name='audio_get_all'),

]

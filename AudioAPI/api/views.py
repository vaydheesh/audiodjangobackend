from django.db.utils import IntegrityError
from django.http.response import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from . import ConstStr
from . import bad_request
from .mappings import ModelMap
from .mappings import SerializerMap


# audio refers to generic audioFileType i.e. Song, Audiobook, Podcast
# Iterating over the audioFileType using if else ladder
# causes code and logic duplication
# So a dictionary with key=audioFileType &
# value=Model/Serializer is used
class AudioResponse(APIView):
    file_type_kwarg = 'audioFileType'
    file_id_kwarg = 'audioFileID'

    def check_data(self, file_type, file_id):
        """
        Check integrity of data
        :param file_type: One of the valid file types
        :param file_id: unique id of file
        :return: None if bad request, else queried object
        """
        if file_type not in SerializerMap:
            return None
        try:
            existing_object = ModelMap[file_type].objects.get(id=file_id)
        except ModelMap[file_type].DoesNotExist:
            return None

        return existing_object

    def get(self, request, *args, **kwargs):
        file_type = kwargs.get(self.file_type_kwarg, None)
        file_id = kwargs.get(self.file_id_kwarg, None)
        existing_object = self.check_data(file_type, file_id)
        if existing_object is None:
            return bad_request()

        serializer = SerializerMap[file_type](existing_object)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        file_type = kwargs.get(self.file_type_kwarg, None)
        file_id = kwargs.get(self.file_id_kwarg, None)
        data = JSONParser().parse(request)
        try:
            meta_data = data[ConstStr.meta_data]
        except KeyError:
            return bad_request()

        existing_object = self.check_data(file_type, file_id)
        if existing_object is None:
            return bad_request()

        serializer = SerializerMap[file_type](existing_object, data=meta_data)
        if serializer.is_valid():
            try:
                serializer.save()
            except IntegrityError:
                return bad_request()

            return JsonResponse(serializer.data, status=status.HTTP_200_OK)
        return bad_request()

    def delete(self, request, *args, **kwargs):
        file_type = kwargs.get(self.file_type_kwarg, None)
        file_id = kwargs.get(self.file_id_kwarg, None)
        existing_object = self.check_data(file_type, file_id)
        existing_object.delete()
        return JsonResponse({'message': 'Deleted'}, status=status.HTTP_200_OK)


@api_view(['GET'])
def audio_get_all(request, audioFileType):
    if not audioFileType:
        return bad_request()
    if audioFileType not in SerializerMap:
        return bad_request()

    songs = ModelMap[audioFileType].objects.all()
    songs_serializer = SerializerMap[audioFileType](songs, many=True)

    return JsonResponse(songs_serializer.data, safe=False)


@api_view(['POST'])
def audio_post(request):
    data = JSONParser().parse(request)
    try:
        file_type = data[ConstStr.file_type]
        meta_data = data[ConstStr.meta_data]
    except KeyError:
        return bad_request()

    if file_type not in SerializerMap:
        return bad_request()

    serializer = SerializerMap[file_type](data=meta_data)
    if serializer.is_valid():
        try:
            serializer.save()
        except IntegrityError:
            return bad_request()

        return JsonResponse(
            serializer.data,
            status=status.HTTP_201_CREATED
        )

    return bad_request()
